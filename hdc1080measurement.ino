
#include <Wire.h>

#define PRINT_TEMP 1


// TEMP SENSOR REQUIREMENTS
#include "ClosedCube_HDC1080.h"
ClosedCube_HDC1080 hdc1080;

// PID REQUIREMENTS
#include <AutoPID.h>
#define OUTPUT_MIN 0
#define OUTPUT_MAX 255
#define KP 150
#define KI .002
#define KD 0

double setPoint = 30;
double outputVal;
double temperature;
AutoPID myPID(&temperature, &setPoint, &outputVal, OUTPUT_MIN, OUTPUT_MAX, KP, KI, KD);
unsigned long lastTempUpdate; //tracks clock time of last temp update


// SCREEN REQUIREMENTS
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define SCREEN_ADDRESS 60
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);


#define PIN_HEAT 12


int heatOn = 0;

void setup()
{
  Serial.begin(115200);

  pinMode(PIN_HEAT, OUTPUT);

  // Init Heater
  hdc1080.begin(0x40);

  // Init PID
  myPID.setTimeStep(200);

  // Init Screen
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(20, 20);
  display.print("HELLO");
  display.display();
}

void loop()
{
  temperature = hdc1080.readTemperature();
  myPID.run(); //call every loop, updates automatically at certain time interval
  SetHeat(int(outputVal));
    display.clearDisplay();
    
    
    display.setTextSize(1);
    display.setCursor(20, 0);
    display.print("TGT: ");
    display.print(setPoint);

    display.setTextSize(2);
    display.setCursor(20, 20);
    display.print(temperature);
    display.print((char)247); // degree symbol 
    display.print("C");

    display.setTextSize(1);
    display.setCursor(20, 48);
    display.print("PWR: ");
    display.print(int(outputVal));
    display.display();

  if (PRINT_TEMP) {
    Serial.print(temperature);
    Serial.print("\t");
    Serial.print(setPoint);
    Serial.print("\t");
    Serial.println(heatOn / 10); // remap val to 0-25 for Serial Plotter
  }
}

void SetHeat(int val) {
  analogWrite(PIN_HEAT, val);
  heatOn = val;
}

void HeatOff() {
  heatOn = 0;
  digitalWrite(PIN_HEAT, 0);
}

void HeatOn() {
  digitalWrite(PIN_HEAT, 1);
  heatOn = 1;
}
